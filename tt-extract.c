/*
 *	IDOS Timetable Extractor
 *
 *	(c) 2013 Tomas Pokorny <jethro@kam.mff.cuni.cz>
 *
 *	This software can be freely distributed and used according
 *	to the terms of the GNU General Public License.
 */

#include <stdlib.h>
#include <stdio.h>
#include <inttypes.h>
#include <sys/stat.h>
#include <errno.h>
#include <stdbool.h>



FILE * in;

void print_strings(uint32_t offset)
{
	fseek(in,offset,SEEK_SET);
	int32_t count;

	fread(&count,1,sizeof(count),in);
	printf("Will read %d strings\n",count);

	int32_t  * indexes;
	indexes = malloc(sizeof(int32_t)*(count+1));
	

	for (int i=0;i<=count;i++){
		fread(indexes+i,1,sizeof(int32_t),in);
	}
	fseek(in,4,SEEK_CUR);
	
	char buf[4096];
	for (int i=0;i<count;i++){
		fread(buf,indexes[i+1]-indexes[i],1,in);
		for (int j=0; j<indexes[i+1]-indexes[i];j++)
			printf("%c",buf[j]);
		printf("\n");
	}

}

int main(int argc, char * argv[])
{
	in = fopen(argv[1], "r");
	int32_t x;
	int32_t y;
	FILE * out;
	int32_t len;
	int32_t count;
	int32_t next;
	int32_t chunks;
	int32_t blocks;
	uint8_t * int_count;
	char key;
	int nulls;
	uint8_t repeat;
	uint8_t tried;
	int chunk_id;
	chunk_id=0;

	next=0x6B;
	repeat=true;
	tried=false;
	
/*	fclose(fopen("data/strings.dat","w"));
	fclose(fopen("data/stations.dat","w"));*/

	while (repeat)
	{
	fseek(in,next,SEEK_SET);
//	printf("Chunk %d\n",chunk_id);
//	printf("Reading header\n");
	nulls=0;
	do {
		fread(&len,1,sizeof(len),in);
		nulls++;
	} while (len==0);
	fread(&blocks,1,sizeof(blocks),in);
	//int_count = malloc(blocks);
	printf("Chunk: %d, Len: %d, blocks: %d, nulls:%d\n",chunk_id,len,blocks,nulls);
	if (!(len%blocks==0)){
		printf("Skipping, individable\n");

		fseek(in,next+0x15,SEEK_SET);
		fread(&len,1,sizeof(len),in);
		fread(&blocks,1,sizeof(blocks),in);
		if (len%blocks==0){
			printf("Hacked\n");
			next+=0x15;
			continue;
		} 
				
		
		}
	else{	
		next= ftell(in)+len;
		tried=false;
		for (int i=0;i<len;i++)
		{
			uint8_t byte;
			fread(&byte,1,1,in);
			
		}
	/*
		int rec_len;
		rec_len=len/blocks;
		int32_t buf[rec_len];
		for (int i=blocks;i>0;i--){
			fread(buf,1,rec_len,in);
			for (int j=0;j<rec_len;j++)
				printf("%x ",buf[j]);
			printf("\n");
		}*/
	}
	
	if ((next==0x1F3)||(next==0x3975b3))
		next+=0x14;
	if (next==0x4bad7)
		next+=0x2D;	
	if ((next==0x71806)||(next==0x7a374)||(next==0x7a46e)||(next==0x7d046)||(next==0xbf32e)||(next==0xe4efc))
		next+=0x15;	
	if (next==0xbf8d9)
		next+=0x11;
	if (next==0xd9faa)
		next+=0x09;
	if ((next==0xe4fd2)||(next==0x1ca76a)||(next==0x20c02e)||(next==0x39c94f))
		next+=0x20;
	if (next==0x2e1ca5)
		next+=0x10;
	if (next==0x434c0c)
		next+=0x23;
	if (next==0x4c1ff3)
		next+=0x1C;
	if (next==0x4e182f)
		next+=0x18;
	if (next==0x4ebacf)
		break;

// HACK, UNREADED
	if (next==0x399126)
		next=0x39A370;

//	if ((next==0x55193)||(next==0x613ce)||(next==0x61a5c)||(next==0x63129))
//		next+=0x15;
/*	if (next==0x28925)
		next+=0x4;*/

	printf("At: %x, next: %x\n",ftell(in),next);
	if (chunk_id>630) scanf("%c",&key);
	chunk_id++;
	}
/*	print_strings(next);
	next=0xFB;
	print_strings(next);
	next=0x20B;
	print_strings(next);
	printf("At: %x\n",ftell(in));
	
	//Banky
	next=0x4E71C;
	print_strings(next);
	printf("At: %x\n",ftell(in));

	//Bankomaty
	next=0x59824;
	print_strings(next);
	printf("At: %x\n",ftell(in));

	//Bezbariérové stanice
	next=0x6184C;
	print_strings(next);
	printf("At: %x\n",ftell(in));

	// Divadla
	next=0x622CA;
	print_strings(next);
	printf("At: %x\n",ftell(in));

	//Hotely
	next=0x676AA;
	print_strings(next);
	printf("At: %x\n",ftell(in));
	
	//Hotely
	next=0x676AA;
	print_strings(next);
	printf("At: %x\n",ftell(in));*/


}


