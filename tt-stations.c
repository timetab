/*
 *	IDOS Stations Extractor
 *
 *	(c) 2013 Tomas Pokorny <jethro@kam.mff.cuni.cz>
 *
 *	This software can be freely distributed and used according
 *	to the terms of the GNU General Public License.
 */

#include <stdlib.h>
#include <stdio.h>
#include <inttypes.h>
#include <sys/stat.h>
#include <errno.h>
#include <stdbool.h>



FILE * in;

void print_stations(uint32_t offset)
{
	fseek(in,offset,SEEK_SET);
	int32_t len;
	int32_t count;
	int32_t mincount;

	fread(&len,1,sizeof(len),in);
	fread(&count,1,sizeof(count),in);
	offset=ftell(in)+len;
	mincount=count;
	printf("Will read %d stations\n",count);

	int32_t  * indexes;
	indexes = malloc(sizeof(int32_t)*(count+1));
	

	for (int i=0;i<count;i++){
		fread(indexes+i,1,sizeof(int32_t),in);
	}
	indexes[count]=indexes[count-1];
	
	int32_t foo;
	fread(&len,1,sizeof(len),in);
	fread(&foo,1,sizeof(count),in);
	offset=ftell(in)+len;
	
	char ** buf;
	buf = malloc(sizeof(char *)*count);

	int32_t str_len;
	for (int i=0;i<count;i++){
		str_len = indexes[i+1]-indexes[i];
		buf[i] = malloc(str_len);
		fread(buf[i],str_len,1,in);
	}

	fread(&len,1,sizeof(len),in);
	fread(&count,1,sizeof(count),in);
	offset=ftell(in)+len;
	mincount = (count<mincount)?count:mincount;

	int32_t * map;
	map = malloc(sizeof(int32_t)*count);
	for (int i=0;i<count;i++)
	{
		fread(map+i,1,sizeof(int32_t),in);
	}

	fread(&len,1,sizeof(len),in);
	fread(&count,1,sizeof(count),in);
	offset=ftell(in)+len;
	mincount = (count<mincount)?count:mincount;
	
	int16_t * id;
	id = malloc(sizeof(int16_t)*count);
	for (int i=0;i<count;i++)
	{
		fread(id+i,1,sizeof(int16_t),in);
	}

	fread(&len,1,sizeof(len),in);
	fread(&count,1,sizeof(count),in);
	offset=ftell(in)+len;
	fseek(in,offset,SEEK_SET);	//skip first letter of station

	fread(&len,1,sizeof(len),in);
	fread(&count,1,sizeof(count),in);
	offset=ftell(in)+len;
	fseek(in,offset,SEEK_SET); //skip second letter of station

	fseek(in,4,SEEK_CUR); //null skip

	fread(&len,1,sizeof(len),in);
	fread(&count,1,sizeof(count),in);
	mincount = (count<mincount)?count:mincount;
	
	int16_t * unk1;
	unk1 = malloc(sizeof(int16_t)*count);
	for (int i=0;i<count;i++)
	{
		fread(unk1+i,1,sizeof(int16_t),in);
	}

	fseek(in,12,SEEK_CUR); //null skip

	fread(&len,1,sizeof(len),in);
	fread(&count,1,sizeof(count),in);
	mincount = (count<mincount)?count:mincount;
	printf("Count: %d\n",count);
	
	int16_t * unk2;
	unk2 = malloc(sizeof(int16_t)*count);
	for (int i=0;i<count;i++)
	{
		fread(unk2+i,1,sizeof(int16_t),in);
	}

	
	FILE * out;
	out = fopen("tt-data/stations.dat","w");
	for (int i=0;i<mincount;i++)
	{
		len = indexes[i+1]-indexes[i];
		fprintf(out,"%d ",len);
		for (int j=0; j<len;j++)
			fprintf(out,"%c",buf[i][j]);
		fprintf(out," %d %d %d %d\n",map[i],id[i],unk1[i],unk2[i]);
	}
	fclose(out);



}

int main(int argc, char * argv[])
{
	in = fopen(argv[1], "r");
	int32_t next;

	next=0x207;
	
	print_stations(next);
	exit(0);

}


