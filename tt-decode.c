/*
 *	IDOS Data Decoder
 *
 *	(c) 2000--2001 Martin Mares <mj@ucw.cz>
 *	(c) 2001--2002 Pavel Machek <pavel@ucw.cz>
 *	(c)	2013--2013 Tomas Pokorny <jethro@kam.mff.cuni.cz>
 *
 *	This software can be freely distributed and used according
 *	to the terms of the GNU General Public License.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <time.h>

static char start_date[100];
static unsigned int start_stamp;

/* Loading of blocks */

static void *
load(char *name)
{
  int *p;
  struct stat st;
  int fd;

  fd = open(name, O_RDONLY);
  if (fd < 0) { fprintf(stderr, "open(%s): %m", name); exit(1); }
  if (fstat(fd, &st) < 0) { fprintf(stderr, "stat: %m"); exit(1); }
  p = malloc(st.st_size + sizeof(int));
  *p++ = st.st_size;
  if (read(fd, p, st.st_size) != st.st_size) { fprintf(stderr, "read: %m"); exit(1); }
  close(fd);
  return p;
}

static int
size(void *x)
{
  return ((int *)x)[-1];
}

#define ASSERT(x) if (!(x)) { fprintf(stderr, "Assertion failed: " #x "\n"); exit(1); }

/* Misc */

static unsigned int
gl(unsigned char *x)
{
  return x[0] | (x[1] << 8) | (x[2] << 16) | (x[3] << 24);
}

static char *
ustamp(time_t x)
{
  static char buf[64];
  struct tm *t = localtime(&x);
  strftime(buf, sizeof(buf), "%Y-%m-%d %H:%M:%S", t);
  return buf;
}

static time_t 
tstamp(unsigned x)
{
  return (x-70*365-17)*86400 + 5000;
}

static char *
ystamp(unsigned x, char *fmt)
{
  time_t w = tstamp(x);
  static char buf[64];
  struct tm *t = localtime(&w);
  strftime(buf, sizeof(buf), fmt, t);
  return buf;
}

static char *
xstamp(unsigned x)
{
  return ystamp(x, "%Y-%m-%d");
}

static char *
xstamp2(unsigned x)
{
  return ystamp(x, "y%Y m%m d%Y");
}

static void
string(int *i, char *s, int t)
{
  if (t < 0 || t >= size(i)/4-1)
    printf("<invalid:%08x>", t);
  else
    fwrite(s+i[t], 1, i[t+1] - i[t], stdout);
}

/* Header */

static int langs;
static int time_rs, time_r_off;

static void
dump_hdr(void)
{
  char *hdr = load("0000");
  int *dbni = load("0001");
  char *dbns = load("0002");
  int *copi = load("0003");
  char *cops = load("0004");

  puts("!version 1.1\n");
  printf("; ### DATABASE ###\n");
  ASSERT(hdr[0x3f] == 0x0d && hdr[0x40] == 0x0a && hdr[0x41] == 0x1a);
  printf("; Database: "); string(dbni, dbns, 0); putchar('\n');
  printf("; Copyright: "); string(copi, cops, 0); putchar('\n');
  printf("; Versions: %x %d.%d\n", gl(hdr+0x46), hdr[0x4a], hdr[0x4b]);
  printf("; File created: %s\n", ustamp(gl(hdr+0x42)));
  printf("!valid %s .. %s\n", xstamp2(gl(hdr+0x54)), xstamp2(gl(hdr+0x58)));
  printf("; Time remark range: %s ", xstamp(gl(hdr+0x5c)));
  strcpy(start_date, xstamp2(gl(hdr+0x54)));
  start_stamp=tstamp(gl(hdr+0x54));
  printf("; to %s\n", xstamp(gl(hdr+0x60)));
  time_r_off = gl(hdr+0x5c);
  time_rs = gl(hdr+0x60) - time_r_off + 1;
  printf("; Last update: %s\n", xstamp(gl(hdr+0x50)));
  langs = size(dbni) / 4;
  printf("; Language count: %d\n", langs);
}

/* Stations */

static int *stanami;
static char *stanams;
static int nstats;

static void
dump_stat(void)
{
  int i;
  int *x10 = load("0010");
  unsigned short *x11 = load("0011");
  unsigned short *x14 = load("0014");
  unsigned short *x15 = load("0015");
  short *ctry = load("0016");
  int *ctryi = load("0017");
  char *ctrys = load("0018");
  short *x19 = load("0019");
  int *x19i = load("0020");
  char *x19s = load("0021");
  int *x23 = load("0023");

  puts("\n; ### STATIONS ###\n");
  stanami = load("0008");
  nstats = size(stanami) / 4 - 1;
  stanams = load("0009");
  for (i=0; i<nstats; i++)
    {
      printf("; %08x ", i);
      string(stanami, stanams, i);
      if (size(ctry))
	{
	  printf(" [");
	  if (ctry[i] < 0)
	    printf("?");
	  else
	    string(ctryi, ctrys, ctry[i]);
	  printf("]");
	}  
      if (size(x19))
	{
	  printf(" [");
	  if (x19[i])
	    string(x19i, x19s, x19[i]-1);
	  else
	    printf("?");
	  printf("]");
	}
      putchar('\n');
      printf("; \t%08x %04x %04x %04x %08x\n", x10[i], x11[i], x14[i], x15[i], x23[i]);
    }
}

/* Time remarks */

static unsigned char **time_r;

static void
load_time_r(void)
{
  int i;

  time_r = malloc(sizeof(unsigned char *) * time_rs);
  for (i=0; i<time_rs; i++)
    {
      char x[8];
      sprintf(x, "%d", 7000+i);
      time_r[i] = load(x);
    }
}

static void
timerem(int i)
{
  int j;
  unsigned char week = 0, old_week = 0xff;
  int count = 0;

  printf("D %d %d ", start_stamp, time_rs);

  for (j=0; j<time_rs; j++)
    {
      /* j corresponds to day time_r_off + j */
      unsigned char *z = time_r[j];

      week |= (!!(z[i/8] & (1 << (i%8)))) << (j%7);
      if (!((j+1)%7))
	{
	  if (old_week == week)
	    count++;
	  else 
	    {
   	      if (old_week != 0xff)
	        printf("%d:%x ", count+1, old_week );
	      old_week = week;
	      week = 0;
	      count = 0;
	    }
        }
    }
  printf("%d:%x", count+1, old_week );
}

/* The Timetable */


static void
dump_tt(void)
{
  int *neighi = load("0025");
  unsigned short *neighx = load("0026");
  int *neighd = load("0027");
  int *tti = load("3001");
  char *tts = load("3002");
  int *tnum = load("3004");
  int *tnumi = load("3005");
  char *tnums = load("3006");
  int *tname = load("3008");
  int *tnamei = load("3009");
  char *tnames = load("3010");
  unsigned int *tflag = load("3011");
  int *tabbri = load("4000");
  char *tabbrs = load("4001");
  short *rembl = load("5000");
  int *rembi = load("5001");
  short *rembx = load("5002");
  int *remi = load("5003");
  char *rems = load("5004");
  short *trem = load("5100");
  int *metri = load("6001");
  int *metrx = load("6002");
  short *line = load("6100");
  int *linei = load("6101");
  char *lines = load("6102");
  short *comp = load("6200");
  int *compi = load("6201");
  char *comps = load("6202");
  int time_plus, conn;
  int i,j,k,m;


  puts("; ### TIMETABLE ###\n");
  for(i=0; i<size(tti)/4-1; i++)
    {
      j = tflag[i] & 0x7fffff;
      printf("# ");
      for (k=0; j & (1 << k); k++)
        string(tabbri, tabbrs, k);

      if (tnum[i] >= 0)
	printf("%d", tnum[i]);
      else
	string(tnumi, tnums, ~tnum[i]);
      printf(" %04x\n;", i);
      if (size(tname))
	{
	  printf(" \"");
	  if (tname[i] >= 0)
	    printf("%d", tname[i]);
	  else
	    string(tnamei, tnames, ~tname[i]);
	  putchar('"');
	}
      printf(" flag=%08x\n", tflag[i]);

      time_plus = 0;
      conn = i;

    retry:
      {
      unsigned short *z;
      int j,k,w;
      int last = -1;
      int cud = 0;

      for(j=tti[conn], w=0; j<tti[conn+1]; j+=4, w++)
        {
	  if(j > (size(tts))) {
	    fprintf(stderr, "Pointer to real data out of range?\n");
	    printf("; Error: Pointer to real data out of range!\n");
	    break;
	  }
	  z = (short *)(tts+j);
	  if ((z[0] & 0x800) && j == tti[conn])
	    {
	      printf("; -> %x + %d\n", ((z[0] & 0xf000) << 4) | z[1], z[0] & 0x7ff);
	      conn = ((z[0] & 0xf000) << 4) | z[1];
	      time_plus = z[0] & 0x7ff;
	      goto retry;
	    }
	  else
	    {
	      k = (z[0] & 0x7ff) + time_plus;
	      if (k > 24*60) {
		      static int warn;
		      if (!warn)
			      fprintf(stderr, "Warning - invalid time:\t%2d:%02d\n", k/60, k%60); 
		      warn = 1;
		     // k = k % (24*60);
	      }
	      printf("\t%d\t", k);

	    /*  if (size(metri) && metri[conn] < metri[conn+1])
		{
		  if (metri[conn] + 4*w < metri[conn+1])
		    printf("%f", (double) metrx[(metri[conn]+w)/4] / 1000);
		  else
		    printf("-");
		}
	      else
		{
#if 0
		  if (w && last != z[1])
		    {
		      int nei;
		      for (nei=neighi[last]; nei<neighi[last+1]; nei++)
			if (neighx[nei] == z[1])
			  break;
		      if (nei < neighi[last+1])
			{
			  cud += neighd[nei];
			  // printf("%d:", neighd[nei]);
			}
		      else
			ASSERT(0);
		    }
#endif
		  printf("%f", (double) cud/1000);
		}*/
	      putchar('\t');
		  printf("%d\t", z[1]);
	     // string(stanami, stanams, z[1]);
	      last = z[1];
	      if (z[0] & 0xf800) printf(" %04x", z[0] & 0xf800);
	      putchar('\n');
	    }
	}
      }

      if (size(line))
	{
	  printf("L");
	  string(linei, lines, line[i]);
	  putchar('\n');
	}
      if (size(comp))
	{
	  printf("G");
	  string(compi, comps, comp[i]);
	  putchar('\n');
	}

      timerem(trem[i]);
      putchar('\n');

      if (size(rembl))
	{
	  k = rembl[i];
	  for (j=rembi[k]; j<rembi[k+1]; j++)
	    {
	      m = rembx[j];
	      printf("B%x ", m);
	      string(remi, rems, m);
	      putchar('\n');
	    }
	}
    }
}

/* main */

int main(void)
{
  dump_hdr();
  dump_stat();
  load_time_r();
  dump_tt();
  return 0;
}
