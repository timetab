CFLAGS=-O2 -Wall -Wno-parentheses -g
LDFLAGS=

all: extract tt-decode

clean:
	rm -f *.o extract tt-decode *~
	rm -rf ext dec data

