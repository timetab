/*
 *	IDOS Map Extractor
 *
 *	(c) 2013 Tomas Pokorny <jethro@kam.mff.cuni.cz>
 *
 *	This software can be freely distributed and used according
 *	to the terms of the GNU General Public License.
 */

#include <stdlib.h>
#include <stdio.h>
#include <inttypes.h>
#include <sys/stat.h>
#include <errno.h>
#include <stdbool.h>



FILE * in;

int main(int argc, char * argv[])
{
	in = fopen(argv[1], "r");
	int32_t x;
	int32_t y;
	FILE * out;
	int32_t len;
	int32_t count;
	int32_t next;
	int32_t chunks;
	int32_t blocks;
	uint8_t * int_count;
		char key;
		int nulls;

	next=0x89;
	fclose(fopen("map/chunks.dat","w"));
	fclose(fopen("map/stations.dat","w"));

	fseek(in,next,SEEK_SET);
	printf("Reading header\n");
	fread(&len,1,sizeof(len),in);
	fread(&blocks,1,sizeof(blocks),in);
	int_count = malloc(blocks);
	for (int i=0;i<blocks;i++)
		int_count[i]=2;
	printf("Len: %d, blocks: %d\n",len,blocks);
	next = ftell(in)+len;
	if (!(len%blocks==0))
		printf("Skipping, header had changed.\n");
	else{
		
		int rec_len;
		rec_len=len/blocks;
		int32_t buf[rec_len];
		for (int i=blocks;i>0;i--){
			fread(buf,1,rec_len,in);
			for (int j=0;j<rec_len;j++)
				printf("%x ",buf[j]);
			printf("\n");
		}
	}
	
	// HACK
	int_count[5]=int_count[6]=int_count[7]=3;
	// /HACK
	
	for (int blkid=0;blkid<blocks;blkid++)
	{
		fseek(in,next,SEEK_SET);
		
		nulls=0;

		do {
			fread(&chunks,1,sizeof(chunks),in);
			nulls++;
			next+=4;
		}
		while (chunks==0);
		
		printf("Read %d null ints, will read %d chunks\n",nulls,chunks);
		if (int_count[blkid]==3)
		{
			next = ftell(in)-4;
			fseek(in,next,SEEK_SET);
			printf("Reading stations\n");
			fread(&len,1,sizeof(len),in);
			fread(&count,1,sizeof(count),in);
			next=ftell(in)+len;

			printf("Base: %x Next: %x, count: %d\n",(uint32_t)ftell(in),len,count);
			
			int32_t idx;
			out = fopen("map/stations.dat","a");
			for (int i=count;i>0;i--)
			{
				fread(&x,1,sizeof(x),in);
				fread(&y,1,sizeof(y),in);
				fread(&idx,1,sizeof(idx),in);
				fprintf(out,"%d %d %d\n",x,y,idx);
			}
			fprintf(out,"\n");
			fclose(out);
		} else {
		
		printf("Reading %d chunks\n",chunks);
		for (int j=chunks;j>0;j--)
		{
			fseek(in,next,SEEK_SET);
			fread(&len,1,sizeof(len),in);
			fread(&count,1,sizeof(count),in);
			next=ftell(in)+len;

			printf("Base: %x Next: %x, count: %d\n",(uint32_t)ftell(in),len,count);
			
			out = fopen("map/chunks.dat","a");
			for (int i=count;i>0;i--)
			{
				fread(&x,1,sizeof(x),in);
				fread(&y,1,sizeof(y),in);
				fprintf(out,"%d %d\n",x,y);
			}
			fprintf(out,"\n");
			fclose(out);

		}
		}
		
	
	} 	
	printf("At: %x continue?",(uint32_t)ftell(in));
	scanf("%c",&key);

	fseek(in,next,SEEK_SET);
	printf("Reading header\n");
	do {
		fread(&len,1,sizeof(len),in);
		nulls++;
		next+=4;
	}
	while (len==0);
	printf("Read %d null ints, will read header of length %x \n",nulls,len);

	fread(&blocks,1,sizeof(blocks),in);
	int_count = malloc(blocks);
	for (int i=0;i<blocks;i++)
		int_count[i]=2;
	printf("Len: %d, blocks: %d\n",len,blocks);
	next = ftell(in)+len;
	if (!(len%blocks==0))
		printf("Skipping, header had changed.\n");
	else{
		
		int rec_len;
		rec_len=len/blocks;
		int32_t buf[rec_len];
		for (int i=blocks;i>0;i--){
			fread(buf,1,rec_len,in);
			for (int j=0;j<rec_len;j++)
				printf("%x ",buf[j]);
			printf("\n");
		}
	}
	
	// HACK
	int_count[0]=3;
	// /HACK
	
	for (int blkid=0;blkid<blocks;blkid++)
	{
		fseek(in,next,SEEK_SET);
		
		nulls=0;

		do {
			fread(&chunks,1,sizeof(chunks),in);
			nulls++;
			next+=4;
		}
		while (chunks==0);
		
		printf("Read %d null ints, will read %d chunks\n",nulls,chunks);
		if (int_count[blkid]==3)
		{
			next = ftell(in)-4;
			fseek(in,next,SEEK_SET);
			printf("Reading stations\n");
			fread(&len,1,sizeof(len),in);
			fread(&count,1,sizeof(count),in);
			next=ftell(in)+len;

			printf("Base: %x Next: %x, count: %d\n",(uint32_t)ftell(in),len,count);
			
			int32_t idx;
			out = fopen("map/stations.dat","a");
			for (int i=count;i>0;i--)
			{
				fread(&x,1,sizeof(x),in);
				fread(&y,1,sizeof(y),in);
				fread(&idx,1,sizeof(idx),in);
				fprintf(out,"%d %d %d\n",x,y,idx);
			}
			fprintf(out,"\n");
			fclose(out);
		} else {
		
		printf("Reading %d chunks\n",chunks);
		for (int j=chunks;j>0;j--)
		{
			fseek(in,next,SEEK_SET);
			fread(&len,1,sizeof(len),in);
			fread(&count,1,sizeof(count),in);
			next=ftell(in)+len;

			printf("Base: %x Next: %x, count: %d\n",(uint32_t)ftell(in),len,count);
			
			out = fopen("map/chunks.dat","a");
			for (int i=count;i>0;i--)
			{
				fread(&x,1,sizeof(x),in);
				fread(&y,1,sizeof(y),in);
				fprintf(out,"%d %d\n",x,y);
			}
			fprintf(out,"\n");
			fclose(out);

		}
		}
		
	
	} 	


	printf("At: %x continue?",(uint32_t)ftell(in));
}

